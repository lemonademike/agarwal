<?php
/* Shortcodes */
function lemonade_services_shortcode( $atts ) {
	extract( shortcode_atts( array(
		'order'						=> 'ASC',
		'orderby'					=> 'menu_order',
		'meta_key'				=> '',
		'posts_per_page'	=> '-1', 
		'display'					=> 'standard',
		'terms_list' 			=> '',
		'by_term'					=> '0',
		'service_type' 		=> '',
	), $atts ) );

	$terms_array = explode(",", $service_type);

	$db_args = array(
		'post_type' 			=> 'services',
		'order'						=> $order,
		'orderby'					=> $orderby,
		'meta_key'				=> $meta_key,
		'posts_per_page' 	=> $posts_per_page, 
		'by_term'					=> $by_term,
		'service_type' 		=> $service_type,
	);


	if($service_type != ""){
		$db_args['tax_query'][] = array(
			array(
				'taxonomy' => 'service_type',
				'field'    => 'slug',
				'terms'    => $terms_array,
			),
		);		
	}

	$services_loop = new WP_Query( $db_args );

	$content = '';

	if($services_loop->have_posts()) {
		switch($display) {	

		case "accordion":
			$content .= '<div class="panel-group" id="service-accordion">';

			$i = 0;

			while( $services_loop->have_posts() ) : $services_loop->the_post();
			
				if($i == 0) { $first = "in"; } else { $first = ''; }
			
				$content_filtered = get_the_content();
				$content_filtered = apply_filters('the_content', $content_filtered);
				$content_filtered = str_replace(']]>', ']]&gt;', $content_filtered);
				
			    $content .= '<div class="panel panel-default">';
				$content .= '<div class="panel-heading">';
				$content .= '<h3 class="panel-title">';
				$content .= '<a class="accordion-toggle" data-toggle="collapse" data-parent="#service-accordion" href="#collapse'.$i.'">';
				$content .= get_the_title();
				$content .= '</a>';					
				$content .= '</h3>';
				$content .= '</div>'; // end .panel-heading
				$content .= '<div id="collapse'.$i.'" class="panel-collapse collapse '.$first.'">';
				$content .= '<div class="panel-body">'.$content_filtered.'</div>';
				$content .= '</div>'; // end .collapse
				$content .= '</div>'; // end .panel

				$i++;

			endwhile;

			$content .= "</div>"; // end .panel-group

			break;


			case "standard":

				$content .= '<div class="service-wrapper">';

				while( $services_loop->have_posts() ) : $services_loop->the_post();

					$content .= '<div class="medium-6 large-3 columns">';
					$content .= '<div class="service-single">';
					$content .= '<a href="'.get_permalink().'">';
					$content .= get_the_post_thumbnail($post->ID, 'doc-thumb');
					$content .= '<p>'.get_the_title().'</p>';
					$content .= '</a>';
					$content .= '</div>';
					$content .= '</div>';

				endwhile;

				$content .= '</div>';

				break;



			case "content":

				$content .= '<div class="service-wrapper">';

				while( $services_loop->have_posts() ) : $services_loop->the_post();

					$content_filtered = get_the_content();
					$content_filtered = apply_filters('the_content', $content_filtered);
					$content_filtered = str_replace(']]>', ']]&gt;', $content_filtered);

					$content .= '<div class="service-single">';
					$content .= '<h3 class="service-title">'.get_the_title().'</h3>';
					$content .= '<div class="service-content">'.$content_filtered.'</div>';
					$content .= '</div>';
				endwhile;

				$content .= '</div>';

				break;

			case "excerpt":

				$content .= '<div class="service-wrapper">';

				while( $services_loop->have_posts() ) : $services_loop->the_post();
					$content .= '<div class="service-single">';
					$content .= '<h3 class="service-title"><a href="'.get_permalink().'">'.get_the_title().'</a></h3>';
					$content .= '<div class="service-excerpt">'.get_the_excerpt().'</div>';
					$content .= '</div>';
				endwhile;

				$content .= '</div>';

				break;

			case "list":

				$content .= '<ul class="services-list ">';

				while( $services_loop->have_posts() ) : $services_loop->the_post();
					$content .= '<li class="service-single">';
					$content .= '<h4 class="service-title"><a href="'.get_permalink().'">'.get_the_title().' <i class="fa fa-long-arrow-alt-right" aria-hidden="true"></i></a></h4>';
					$content .= '</li>';
				endwhile;

				$content .= '</ul>';

				break;

				case "ramon":

				$content .= '<div class="container">';
				$content .= '<div class="row">';

				while( $services_loop->have_posts() ) : $services_loop->the_post();
					$content .= '<div class="col-sm-6">';
					$content .= '<a class="no-underline" href="'.get_permalink().'"><h4 class="title highlight-primary">'.get_the_title().'</h4> <i class="fas fa-long-arrow-alt-right"></i></a>';
					$content .= '</div>';
				endwhile;

				$content .= '</div>';
				$content .= '</div>';

				break;
		}
			
	}

	wp_reset_postdata();
	return $content;
}
add_shortcode( 'lemonade_services', 'lemonade_services_shortcode' );