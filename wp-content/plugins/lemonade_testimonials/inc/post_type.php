<?php
function lemonade_create_testimonials() {
	$labels = array(
		'name'                => 'Testimonials',
		'singular_name'       => 'Testimonials',
		'menu_name'           => 'Testimonials',
		'parent_item_colon'   => 'Parent Testimonial:',
		'all_items'           => 'All Testimonials',
		'view_item'           => 'View Testimonial',
		'add_new_item'        => 'Add New Testimonial',
		'add_new'             => 'New Testimonial',
		'edit_item'           => 'Edit Testimonial',
		'update_item'         => 'Update Testimonial',
		'search_items'        => 'Search Testimonials',
		'not_found'           => 'No Testimonials found',
		'not_found_in_trash'  => 'No Testimonials found in Trash',
	);

	$args = array(
		'label'               => 'Testimonials',
		'description'         => 'Testimonials post type',
		'labels'              => $labels,
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 6,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'capability_type'     => 'page',
		'supports'            => array( 'title', 'excerpt', 'thumbnail', 'revisions', 'page-attributes', 'editor'),
		'menu_icon' => plugins_url( 'lemonade_icon.png', __FILE__ ),
	);

	register_post_type( 'testimonials', $args );
}
add_action( 'init', 'lemonade_create_testimonials', 0 );