<?php
//Testimonial Type
function testimonial_type_init() {
	register_taxonomy(
		'testimonial_type',
		'testimonials',
		array(
			'label' => __( 'Testimonial Type' ),
			'rewrite' => array( 
			'slug' => 'testimonial_type',
			),
		'hierarchical' => true,
		)
	);
}
add_action( 'init', 'testimonial_type_init' ); 